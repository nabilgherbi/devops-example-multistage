#Build the application
FROM maven:3-jdk-11 AS build

WORKDIR /app

COPY . .

RUN mvn clean package -DskipTests

#RUN the application

FROM openjdk:8-jre-slim

WORKDIR /app

COPY --from=build /app/target/*.jar /app/my_app.jar

EXPOSE 2222

ENTRYPOINT ["java","-jar","my_app.jar"]
